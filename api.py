from requests import get as __get
from requests import post as __post
from json import loads, dumps
import uuid_manager as manager


HOST = None
PORT = None

def is_init():
	if not HOST or not PORT:
		raise InitError(
				(
					'Pleace initialize this '
					'application with setup method'
				)
			)


def setup(host:str, port:int) -> None:
	global HOST, PORT

	if not isinstance(host, str):
		raise TypeError('HOST must be contain string')

	if not isinstance(port, int):
		raise TypeError('PORT must be contain integer')

	HOST = host
	PORT = port


def get(uuid:str) -> __get:
	is_init()
	if not manager.is_uuid(uuid):
		return False

	url = f"http://{HOST}:{PORT}/db/get?id={uuid}"

	return __get(url)


def update(uuid:str, data:str) -> __post:
	is_init()

	if not manager.is_uuid(uuid):
		return False

	if not isinstance(data, str):
		return False

	url = f"http://{HOST}:{PORT}/db/save/node?id={uuid}"

	return __post(url, data)


def save(data:str="") -> dict:
	is_init()

	url = f"http://{HOST}:{PORT}/db/save/node"

	response = __post(url, str(data)).content.decode()

	response = loads(response)

	if 'uuid' in response:
		response = response['uuid']

	return response


def delete(uuid:str) -> __get:
	is_init()
	if not manager.is_uuid(uuid):
		return False

	url = f"http://{HOST}:{PORT}/db/delete?id={uuid}"

	return __get(url)


def parse_response(resp:__get) -> str:
	return resp.content.decode()


# Errors

class InitError(Exception):...
