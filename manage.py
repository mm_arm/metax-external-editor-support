#!/usr/bin/python3
from argparse import ArgumentParser
import file_operations as file
import uuid_manager as uuid_m
import os
import api
import config as cfg
import handlers as observers
from colorama import Fore, Back, Style, init

parser = ArgumentParser(
	description=(
		'The Metax extension for using'
		' external editors and metax files'
	)
)

parser.add_argument('-u', '--update',
	help=(
		'[UUID] must you have a file who name contain'
		' uuid for update uuid data to file content'
	),
	type=str
)

parser.add_argument('-e', '--edit',
	help=(
		'[UUID] must you have a file who name contain uuid'
	),
	type=str
)

parser.add_argument('-l', '--load',
	help=(
		'[UUID] to load file'
		' content in current state'
	),
	type=str
)

parser.add_argument('-d', '--diff',
	help=(
		'two uuids or links or filenames to diff example` '
		'link:[url] -to load by web. '
		'metax:[uuid] -to load from metax. '
		'file:[file_name] - to load file. '
		'or only uuid'
	),
	nargs="+",
	type=str
)

parser.add_argument('-i', '--init',
	help=(
		'Init current directory as '
		'a metax util directory'
	),
	action='store_const',
	const=True,
	default=False
)

parser.add_argument('-n', '--new',
	help=(
		'Create the new file and return uuid'
	),
	action='store_const',
	const=True,
	default=False
)

parser.add_argument('-o', '--observe',
	help=(
		'Start observe the -o directory'
	),
	action='store_const',
	const=cfg.PATH
)

args = parser.parse_args()

# INITIALIZE
api.setup(cfg.HOST, cfg.PORT)
uuid_m.UUID_FILE_NAME = os.path.join(cfg.PATH, cfg.UUID_FILE_NAME)
mod_list = {}
init(autoreset=True) # colorama

# DEFINES
def file_change_handler(event):
	global mod_list
	if not event.is_directory:
		name = os.path.basename(event.src_path).split('.')[0]
		if uuid_m.is_uuid(name):
			data = file.read(event.src_path)
			if not name in mod_list:
				mod_list[name] = ""

			if not file.is_equal(file.md5(data), mod_list[name]):
				mod_list[name] = file.md5(data)
				api.update(name, data)
				print(Fore.YELLOW + "UPDATING", event.src_path)



if args.init:
	if uuid_m.init(uuid_m.UUID_FILE_NAME):
		print(Fore.GREEN + f"Success Initialized {os.getcwd()}")
	else:
		print(Fore.RED + "Current Directory already Initialized ")
	exit()


if args.new:
	filename = api.save()
	if uuid_m.is_uuid(filename):
		fn = os.path.join(cfg.PATH, filename)
		file.new(fn)
		uuid_m.new(filename, uuid_m.UUID_FILE_NAME)
		print(Fore.GREEN + f"Success Created {fn}")
	else:
		print("Error Creating New file")
	exit()


if args.edit:
	response = api.get(args.edit.strip())
	if response.status_code == 200:
		response = api.parse_response(response)
		file.save(os.path.join(cfg.PATH, args.edit.strip()), response)
		uuid_m.new(args.edit.strip(), uuid_m.UUID_FILE_NAME)
		print(Fore.GREEN + f"Success loaded {args.edit}")
	else:
		print("Error while loading file")
	exit()


if args.update:
	if uuid_m.is_uuid(args.update.strip()):
		data = file.read(file.choose_file(args.update.strip(), cfg.PATH))
		api.update(args.update.strip(), data)
		print(Fore.GREEN + f"Success Updated {args.update}")
	else:
		print("Updating error")
	exit()


if args.load:
	args.load = args.load.strip()
	if uuid_m.is_uuid(args.load):
		file_name = file.choose_file(args.load, cfg.PATH)
		current_content = file.read(file_name)
		new_content = api.parse_response(api.get(args.load))
		update = not file.is_equal(
					file.md5(current_content),
					file.md5(new_content)
				)

		if update:
			file.save(file_name, new_contentc)
			print(Fore.GREEN + f"Success Loaded {file_name}")
		else:
			print("Files is etual")
	else:
		print("Loading Errors")
	exit()

if args.diff:
	contents = []
	for path in args.diff:
		path = path.split(':', 1)
		if len(path) > 1:
			protocol, path = path
			protocol = protocol.strip().lower()
			if protocol == 'metax':
				data = api.get(path)
				if data.status_code == 200:
					content = data.content.decode()
					contents.append([
							file.md5(content),
							content,
							path
						])

			elif protocol == 'file':
				data = file.read(path)
				contents.append([
						file.md5(data),
						data,
						path
					])
			elif protocol == 'link':
				data = api.__get(path)
				if data.status_code == 200:
					content = data.content.decode()
					contents.append([
							file.md5(content),
							content,
							path
						])
		else:
			data = api.get(path[0])
			if data.status_code == 200:
				content = data.content.decode()
				contents.append([
						file.md5(content),
						content,
						path[0]
					])

	if file.is_equal(*[unit[0] for unit in contents]):
		print("Files is equal")
		exit()

	mode = int(input('Select Show Mode [1, 2]'))

	for index, [md5, data, name] in enumerate(contents):
		for index_, [md5_, data_, name_] in enumerate(contents[index:None]):
			if not file.is_equal(name_, name):
				print(f"Diferences between {name} - {name_}")
				min_ = min(len(name), len(name_))
				max_ = max(len(name), len(name_))
				id_ = len(name) < len(name_)
				spaces = ' '*(max_-min_)
				for diff in file.diff(data, data_):
					print(f'Line:{Fore.GREEN} {diff[0]}')
					if mode == 1:
						print(f"\tDiff: {Fore.YELLOW}'{diff[1]}'")
						print(f"\tDiff: {Fore.BLUE}'{diff[2]}'")
					else:
						print((
							f"\tFile: {Fore.YELLOW}{name}{Style.RESET_ALL}"
							f"{spaces if id_ else ''}"
							f" -> Diff: {Fore.YELLOW}'{diff[1]}'"
						))
						print((
							f"\tFile: {Fore.BLUE}{name_}{Style.RESET_ALL}"
							f"{spaces if not id_ else ''}"
							f" -> Diff: {Fore.BLUE}'{diff[2]}'"
						))


	exit()


if args.observe:
	import time

	x = observers.handle(args.observe, file_change_handler)
	print("Observing ...")

	try:
		while True:
			time.sleep(1)
	except KeyboardInterrupt:
		x.stop()

	x.join()

parser.print_help()

