INT = python3
PACKAGER = pyinstaller
FILE = manage.py
CARGS = --onefile

observe:$(FILE)
	@./$(FILE) --observe

diff:$(FILE)
	@./$(FILE) --diff $(files)

push:$(FILE)
	@./$(FILE) --update $(uuid)

pull:$(FILE)
	@./$(FILE) --load $(uuid)

init:$(FILE)
	@./$(FILE) --init

new:$(FILE)
	@./$(FILE) --new

clone:$(FILE)
	@./$(FILE) --edit $(uuid)

$(FILE):
	@git clone https://gitlab.com/mm_arm/metax-external-editor-support.git

build:$(FILE) installer
	@$(PACKAGER) $(CARGS) $(FILE)

installer:
	@$(INT) -m pip install $(PACKAGER)
	@$(INT) -m pip install https://github.com/pyinstaller/pyinstaller/archive/develop.tar.gz
