import re
import file_operations as file


UUID_PATTERN = re.compile(r'^[\da-f]{8}-([\da-f]{4}-){3}[\da-f]{12}$', re.IGNORECASE)
UUID_FILE_NAME = '.uuidlist'

is_uuid = lambda uuid:(UUID_PATTERN.match(uuid) != None)

def init(action:str=UUID_FILE_NAME) -> bool:
	return file.new(action)


def uuids(action:str=UUID_FILE_NAME) -> list:
	r = []
	for line in file.read(action).splitlines():
		r.append(line.strip())
	return r


def new(uuid:str, to:str=UUID_FILE_NAME) -> bool:
	if is_uuid(uuid):
		file.append(to, f'{uuid}\n')
