from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

_file_mod_handler = lambda *a, **k:...

class __file_handlers(FileSystemEventHandler):
	def __init__(self):
		self.src_path = ''

	def on_modified(self, event):
		if self.src_path == event.src_path:
			return
		else:
			self.src_path = event.src_path
		_file_mod_handler(event)

def handle(filename, handler=None):
	global _file_mod_handler
	if handler:
		_file_mod_handler = handler

	event_handler = __file_handlers()
	observer = Observer()
	observer.schedule(
		event_handler,
		path=filename,
		recursive=False
	)
	observer.start()

	return observer
