from hashlib import md5 as __hash
from os.path import isfile as is_available
from os import listdir

# Create New empty file
def new(action:str) -> bool:
	if not is_available(action):
		open(action, 'w+').close()
		return True
	return False


def choose_file(name:str, path:str=".") -> str:
	filenames = []
	fileId = 0
	for file_name in listdir(path):
		if name in file_name:
			filenames.append(file_name)

	if len(filenames) > 1:
		print("You have a many similar filenames pleace Choose One:")
		for index, name in enumerate(filenames):
			print(index+1, name)
		fileId = int(input('Choose By id: '))-1

	return filenames[fileId]


# Write File content
def save(filename:str, content:str) -> bool:
	with open(filename, 'w+') as f:
		try:
			f.write(str(content))
			return True
		except Exception as e:
			return False


# Append to file content
def append(filename:str, content:str) -> bool:
	with open(filename, 'a') as f:
		try:
			f.write(str(content))
			return True
		except Exception as e:
			return False


# read file content
def read(filename:str) -> str:
	with open(filename, 'r') as f:
		return f.read()


# return list of differences two strings
def diff(text1:str, text2:str) -> str:
	diffs = []
	if not is_equal(md5(text1), md5(text2)):
		text2 = text2.splitlines()
		text1 = text1.splitlines()
		range_  = min(len(text1), len(text2))
		for i in range(range_):
			if not is_equal(text1[i], text2[i]):
				diffs.append([i+1, text1[i], text2[i]])

		max_t = max(text1, text2)
		dir_  = len(text1) > len(text2)

		for i, item in enumerate(max_t[range_:None]):
			if dir_:
				diffs.append([range_+i+1, item, None])
			else:
				diffs.append([range_+i+1, None, item])

	return diffs


# return md5 of your text
def md5(text:str) -> str:
	if isinstance(text, str):
		text = text.encode()

	return __hash(text).hexdigest()


# return two arguments is equal or no
def is_equal(*texts:list) -> bool:
	return len(set(texts)) == 1
